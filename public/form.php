<!DOCTYPE html>
<html lang="ru">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Моя WEB страница</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
         integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
         integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
         crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
         integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
         crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
         integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
         crossorigin="anonymous"></script>
      <link rel="stylesheet" href="style.css">
   </head>
   <Body>
      <div class="container-fluid">
         <header class="row d-flex flex-row justify-content-center">
            <div
               class="d-flex flex-row align-items-center justify-content-around justify-content-sm-start col-sm-9 px-sm-4 h-120">
               <img id="img" src="priroda.png" alt="Logotip">
               <div id="name">WEB страница back 5 </div>
            </div>
            <div>
               <?php
                  if(empty($_SESSION['login']))
                  print('<a id="in" href="/back5/public/login.php">ВОЙТИ</a>');
                  ?>
            </div>
         </header>
         <div class="items d-flex flex-column">
            <div class="row d-flex flex-row justify-content-center mt-3 order-sm-1">
               <div class="col-sm-9">
                  <div class="items d-flex flex-column">
                     <div class="row d-flex flex-row justify-content-center mt-3 order-sm-3">
                        <div class="col-sm-9 bg-light">
                           <div class="items d-flex flex-column ">
                              <div id="form">
                                 <H1> Форма </H1>
                                 <?php
                                    if (!empty($messages)) {
                                      print('<div id="messages">');
                                      foreach ($messages as $message) {
                                        print($message);
                                        }
                                      print('</div>');
                                    }
                                    ?>
                                 <BR>
                                 <form method="POST" action="">
                                    <label>
                                    Имя:<br />
                                    <input name="firstname" <?php if ($errors['firstname']) {print "class='error'" ;} ?> value="<?php print $values['firstname']; ?>" />
                                    </label><br />
                                    <label>
                                    E-mail:<br />
                                    <input name="email" <?php if ($errors['email']) {print 'class="error"' ;} ?> value="<?php print $values['email']; ?>"  />
                                    </label><br />
                                    <label>
                                    Дата Рождения:<br />
                                    <input name="date" <?php if ($errors['date']) {print 'class="error"' ;} ?> value="<?php print $values['date']; ?>" />
                                    </label><br />
                                    <p>Пол </p>
                                    <label><input type="radio" name="sex" <?php if ($errors['sex']) {print 'class="error"' ;} if($values['sex']=="1"){print "checked='checked'";}?> value="1" />
                                    Мужской</label>
                                    <label><input type="radio" name="sex" <?php if ($errors['sex']) {print 'class="error"' ;} if($values['sex']=="2"){print "checked='checked'";}?> value="2" />
                                    Женский</label>
                                    <p>Количество конечностей</p>
                                    <label>
                                    <input type="radio" name="kolvo" <?php if ($errors['kolvo']) {print 'class="error"' ;} if($values['kolvo']=="All"){print "checked='checked'";}?> value="All" />Все 
                                    </label>
                                    <label>
                                    <input type="radio" name="kolvo" <?php if ($errors['kolvo']) {print 'class="error"' ;} if($values['kolvo']=="NotAll"){print "checked='checked'";}?> value="NotAll" />Не Все
                                    </label>
                                    <BR>
                                    <BR>
                                    <label>
                                       Сверхспособности:
                                       <br>
                                       <select name="superpower" id="superpower" style="width: 300px;" >
                                          <option value="Бессмертие" <?php if($values['superpower']=="Бессмертие"){print "selected='selected'";}?>>Бессмертие</option>
                                          <option value="Прохождение сквозь стены" <?php if($values['superpower']=="Прохождение сквозь стены"){print "selected='selected'";}?>>Прохождение сквозь стены</option>
                                          <option value="Левитация" <?php if($values['superpower']=="Левитация"){print "selected='selected'";}?>>Левитация</option>
                                       </select>
                                    </label>
                                    <br>
                                    <br>
                                    <label>Биография:
                                    <textarea name="comment" <?php if ($errors['comment']) {print 'class="error"' ;} ?>><?php print $values['comment']; ?></textarea>
                                    </label>
                                    <p>
                                       <label><span class="formTextRed">*</span>
                                       <input type="checkbox" name="confirm" <?php if($values['confirm']=="1"){print "checked='checked'";}?> value="1" />
                                       С контрактом ознакомлен</label>
                                    </p>
                                    <p class="submit">
                                       <input type="image" SRC="1.jpg" alt="текст" onMouseOver="this.src='2.jpg'"
                                          onMouseOut="this.src='1.jpg'" />
                                    </p>
                                    <?php
                                       if(!empty($_SESSION['login']))
                                       print('<input type="submit" name="exit" value="Выйти" />');
                                       ?>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <footer class="row d-flex flex-row justify-content-start mt-3 h-sm-120">
            <div class="d-flex flex-row align-items-center col-sm-9">
               <div>(с) Екатерина Харичева 2021 </div>
            </div>
         </footer>
      </div>
   </body>
</html>