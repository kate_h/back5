<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();

  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';

    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }

  $errors = array();
  $errors['firstname'] = !empty($_COOKIE['firstname_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['kolvo'] = !empty($_COOKIE['kolvo_error']);
  $errors['superpower'] = !empty($_COOKIE['superpower_error']);
  $errors['comment'] = !empty($_COOKIE['comment_error']);
  $errors['confirm'] = !empty($_COOKIE['confirm_error']);
  $errors['1'] = !empty($_COOKIE['1_error']);
  $errors['2'] = !empty($_COOKIE['2_error']);
  $errors['3'] = !empty($_COOKIE['3_error']);

  if ($errors['firstname']) {
    setcookie('firstname_error', '', 100000);
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages[] = '<div class="error">Заполните e-mail.</div>';
  }
  if ($errors['date']) {
    setcookie('date_error', '', 100000);
    $messages[] = '<div class="error">Заполните дату.</div>';
  }
  if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
    $messages[] = '<div class="error">Выберите один варинт.</div>';
  }
  if ($errors['kolvo']) {
    setcookie('kolvo_error', '', 100000);
    $messages[] = '<div class="error">Выберите один варинт.</div>';
  }
  if ($errors['superpower']) {
    setcookie('superpower_error', '', 100000);
    $messages[] = '<div class="error">Выберите один варинт.</div>';
  }
  if ($errors['comment']) {
    setcookie('comment_error', '', 100000);
    $messages[] = '<div class="error">Заполните биографию.</div>';
  }
  if ($errors['confirm']) {
    setcookie('confirm_error', '', 100000);
    $messages[] = '<div class="error">Примите условия.</div>';
  }
  if ($errors['1']){
    setcookie('1_error', '', 100000);
    $messages[] = '<div class="error">Имя должно быть написано при помощи латинского алфавита без добавления лишних знаков</div>';
  } 
  if ($errors['2']){
    setcookie('2_error', '', 100000);
    $messages[] = '<div class="error">Формат эл почты имеет вид: example@email.com</div>';
  } 
  if ($errors['3']){
    setcookie('3_error', '', 100000);
    $messages[] = '<div class="error">Формат даты 02.02.2021</div>';
  } 

  $values = array();
  $values['firstname'] = empty($_COOKIE['firstname_value']) ? '' : $_COOKIE['firstname_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
  $values['kolvo'] = empty($_COOKIE['kolvo_value']) ? '' : $_COOKIE['kolvo_value'];
  $values['superpower'] = empty($_COOKIE['superpower_value']) ? '' : $_COOKIE['superpower_value'];
  $values['comment'] = empty($_COOKIE['comment_value']) ? '' : $_COOKIE['comment_value'];
  $values['confirm'] = empty($_COOKIE['confirm_value']) ? '' : $_COOKIE['confirm_value'];

  $flag = FALSE;
  foreach($errors as $er){
    if(!empty($er)){
      $flag = TRUE;
      break;
    }
    print($er);
  }

  if (!$flag && !empty($_COOKIE[session_name()]) &&
  session_start() && !empty($_SESSION['login'])) { 
        try {
        $user = 'u24108';
        $pass1 = '3343567';
        $db = new PDO('mysql:host=localhost;dbname=u24108', $user, $pass1, array(PDO::ATTR_PERSISTENT => true));
        $log1 = $_SESSION['login'];
        $pass24 = $_SESSION['pass'];
          $data = $db->query("SELECT * FROM form2 where login = '$log1' AND pass='$pass24'");  

          foreach ($data as $row) {
            $values['firstname'] = $row['name'];
            $values['email'] = $row['email'];
            $values['date'] = $row['date'];
            $values['sex'] = $row['radio1'];
            $values['kolvo'] = $row['radio2'];
            $values['superpower'] = $row['fieldID'];
            $values['comment'] = $row['name2'];
            $values['confirm'] = $row['check3'];
        }
      } catch(PDOException $e) {
          echo 'Ошибка: ' . $e->getMessage();
      }
    
    printf('Вход с логином %s', $_SESSION['login']);
  }

  include('form.php');
}

else {
  $errors = FALSE;
  if (!preg_match("/^[-a-zA-Z]+$/",$_POST['firstname'])){
    setcookie('1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/",$_POST['email'])){
    setcookie('2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/^(\d{1,2})\.(\d{1,2})(?:\.(\d{4}))?$/",$_POST['date'])){
    setcookie('3_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['firstname'])) {
    setcookie('firstname_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('firstname_value', $_POST['firstname'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['date'])) {
    setcookie('date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('date_value', $_POST['date'], time() + 365 * 24 * 60 * 60);
  } 
  if (empty($_POST['sex'])) {
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['kolvo'])) {
    setcookie('kolvo_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('kolvo_value', $_POST['kolvo'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['superpower'])) {
    setcookie('superpower_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('superpower_value', $_POST['superpower'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['comment'])) {
    setcookie('comment_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('comment_value', $_POST['comment'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['confirm'])) {
    setcookie('confirm_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('confirm_value', $_POST['confirm'], time() + 365 * 24 * 60 * 60);
  }

  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('1_error', '', 100000);
    setcookie('2_error', '', 100000);
    setcookie('3_error', '', 100000);
    setcookie('firstname_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('kolvo_error', '', 100000);
    setcookie('superpower_error', '', 100000);
    setcookie('comment_error', '', 100000);
    setcookie('confirm_error', '', 100000);
  }
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    $name = $_POST['firstname'];
    $email = $_POST['email'];
    $date = $_POST['date'];
    $radio1 = $_POST['sex'];
    $radio2 = $_POST['kolvo'];
    $field4 = $_POST['superpower'];
    $name2 = $_POST['comment'];
    $check = $_POST['confirm'];
    
    $user = 'u24108';
    $pass1 = '3343567';
    $db = new PDO('mysql:host=localhost;dbname=u24108', $user, $pass1, array(PDO::ATTR_PERSISTENT => true));
    $log1 = $_SESSION['login'];
    $pass24 = $_SESSION['pass'];
    
    try { 
      $stmt = $db->prepare("UPDATE form2 SET name='$name',email='$email',date='$date',radio1='$radio1',radio2='$radio2',fieldID='$field4',name2='$name2',check3='$check' where login = '$log1' AND pass='$pass24'");
      $stmt -> execute();
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
  }
  else {
    $login = uniqid();
    $pass = rand();
    $pass2 = md5($pass);
    setcookie('login', $login);
    setcookie('pass', $pass);

    $name = $_POST['firstname'];
    $email = $_POST['email'];
    $date = $_POST['date'];
    $radio1 = $_POST['sex'];
    $radio2 = $_POST['kolvo'];
    $field4 = $_POST['superpower'];
    $name2 = $_POST['comment'];
    $check = $_POST['confirm'];
  
  $user = 'u24108';
  $pass1 = '3343567';
  $db = new PDO('mysql:host=localhost;dbname=u24108', $user, $pass1, array(PDO::ATTR_PERSISTENT => true));
  
  try {
    $stmt = $db->prepare("INSERT INTO form2 (name,email,date,radio1,radio2,fieldID,name2,check3,hash,login,pass) VALUE (:name,:email,:date,:radio1,:radio2,:field4,:name2,:check3,:hash,:login,:pass)");
    $stmt -> execute(['name'=>$name,'email'=>$email,'date'=>$date,'radio1'=>$radio1,'radio2'=>$radio2,'field4'=>$field4,'name2'=>$name2,'check3'=>$check,'hash'=>$pass2,'login'=>$login,'pass'=>$pass]);
  }
  catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
  }
  }

  setcookie('save', '1');

  header('Location: index.php');
}
if(isset($_POST['exit'])){
  session_destroy();
  header('Location: login.php');
}

